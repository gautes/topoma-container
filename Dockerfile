FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /config/requirements.pip /config/
RUN pip install -r /config/requirements.pip
RUN mkdir /src;
EXPOSE 8000
WORKDIR /src
CMD tar zxf z2.tar.gz && \
    python manage.py makemigrations && \
    python manage.py migrate && \
    echo 'loading data ...' && \
    python manage.py loaddata databackup/materials.json && \
    echo 'starting server ...' && \
    gunicorn z2.wsgi:application -b 0.0.0.0:8000 \
             --workers 3 \ 
             --log-level=debug --log-file=gunicorn.log \
             --access-logfile=access.log  
