(function() {
 
var defaultConfig = {
           hover: {
                   mode: 'single'
           },

           scales: {
                   xAxes: [{
                           type: 'linear', // scatter should not use a category axis
                           position: 'bottom',
                           id: 'x-axis-1' // need an ID so datasets can reference the scale
                   }],
                   yAxes: [{
                           type: 'linear',
                           position: 'left',
                           id: 'y-axis-1'
                   }]
           },

           tooltips: {
                   callbacks: {
                           title: function() {
                                   // Title doesn't make sense for scatter since we format the data as a point
                                   return '';
                           },
                           label: function(tooltipItem) {
                                   return '(' + tooltipItem.xLabel + ', ' + tooltipItem.yLabel + ')';
                           }
                   }
           }
}


var KScale = Chart.LinearScaleBase.extend({

		buildTicks: function() {
			var me = this;
			var chart = me.chart;
			var opts = me.options;
			var data = chart.data;
			var datasets = data.datasets;
			var labels = data.labels;
			var tickOpts = me.options.ticks;

			// Figure out what the max number of ticks we can support it is based on the size of
			// the axis area. For now, we say that the minimum tick spacing in pixels must be 50
			// We also limit the maximum number of ticks to 11 which gives a nice 10 squares on
			// the graph. Make sure we always have at least 2 ticks
			var maxTicks = me.getTickLimit();
			maxTicks = Math.max(2, maxTicks);

			var numericGeneratorOptions = {
				maxTicks: maxTicks,
				min: tickOpts.min,
				max: tickOpts.max,
				stepSize: Chart.helpers.getValueOrDefault(tickOpts.fixedStepSize, tickOpts.stepSize)
			};
			//var ticks = me.ticks = Chart.Ticks.generators.linear(numericGeneratorOptions, me);
                        var ticks = me.ticks =  []
                        var tickslabels = me.tickslabels =  []
                        var lastx=-1.0;
                        var lastlab=""
                        var kthr=0.01;
                        for (var k in labels){
                             lab=labels[k].label;
                             if(lab=="\\Gamma"){ lab="\u0393" }
                             if(lab=="\\Sigma"){ lab="\u03A3" }
                             if(lab=="\\Sigma_1"){ lab="\u03A3_1" }
                             x=datasets[0].data[k].x;
                             if(lab != ""){
                                  if((x - lastx) >= kthr ){
                                     ticks.push(x);
                                     tickslabels.push(lab);
                                     lastx=x;
                                     lastlab=lab;
                                   }
                                  if((x - lastx) < kthr && lastlab != lab){
                                     ticks.pop();
                                     ticks.push(x);
                                     tickslabels.pop();
                                     tickslabels.push(lastlab+'|'+lab);
                                     lastx=x;
                                     lastlab=lab;
                                   }

                             }
 
                       }

			// At this point, we need to update our max and min given the tick values since we have expanded the
			// range of the scale
			me.max = Chart.helpers.max(ticks);
			me.min = Chart.helpers.min(ticks);
		        me.start = me.min;
			me.end = me.max;
			},
		

		determineDataLimits: function() {
			var me = this;
			var opts = me.options;
			var chart = me.chart;
			var data = chart.data;
			var datasets = data.datasets;
			var isHorizontal = me.isHorizontal();
			var DEFAULT_MIN = 0;
			var DEFAULT_MAX = 1;

			function IDMatches(meta) {
				return isHorizontal ? meta.xAxisID === me.id : meta.yAxisID === me.id;
			}

			// First Calculate the range
			me.min = null;
			me.max = null;

			var hasStacks = opts.stacked;
			if (hasStacks === undefined) {
				Chart.helpers.each(datasets, function(dataset, datasetIndex) {
					if (hasStacks) {
						return;
					}

					var meta = chart.getDatasetMeta(datasetIndex);
					if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta) &&
						meta.stack !== undefined) {
						hasStacks = true;
					}
				});
			}


			me.min = isFinite(me.min) ? me.min : DEFAULT_MIN;
			me.max = isFinite(me.max) ? me.max : DEFAULT_MAX;

			// Common base implementation to handle ticks.min, ticks.max, ticks.beginAtZero
			this.handleTickRangeOptions();
		},

		convertTicksToLabels: function() {
			var me = this;
			// Convert ticks to strings
			me.ticksAsNumbers = me.ticks;
			me.ticks = me.tickslabels;
		},


		getTickLimit: function() {
			var maxTicks;
			var me = this;
			var tickOpts = me.options.ticks;

			if (me.isHorizontal()) {
				maxTicks =   Math.min(tickOpts.maxTicksLimit ? tickOpts.maxTicksLimit : 11, Math.ceil(me.width / 50));
			} else {
				// The factor of 2 used to scale the font size has been experimentally determined.
				var tickFontSize = Chart.helpers.getValueOrDefault(tickOpts.fontSize, Chart.defaults.global.defaultFontSize);
				maxTicks = Math.min(tickOpts.maxTicksLimit ? tickOpts.maxTicksLimit : 11, Math.ceil(me.height / (2 * tickFontSize)));
			}

			return maxTicks;
		},
		// Called after the ticks are built. We need
		handleDirectionalChanges: function() {
			if (!this.isHorizontal()) {
				// We are in a vertical orientation. The top value is the highest. So reverse the array
				this.ticks.reverse();
			}
		},
		getLabelForIndex: function(index, datasetIndex) {
			return +this.getRightValue(this.chart.data.datasets[datasetIndex].data[index]);
              //          console.log('here')
	//		return +this.tickslabels[index]
		},
		// Utils
		getPixelForValue: function(value) {
			// This must be called after fit has been run so that
			// this.left, this.top, this.right, and this.bottom have been defined
			var me = this;
			var start = me.start;

			var rightValue = +me.getRightValue(value);
			var pixel;
			var range = me.end - start;

			if (me.isHorizontal()) {
				pixel = me.left + (me.width / range * (rightValue - start));
				return Math.round(pixel);
			}

			pixel = me.bottom - (me.height / range * (rightValue - start));
			return Math.round(pixel);
		},
		getValueForPixel: function(pixel) {
			var me = this;
			var isHorizontal = me.isHorizontal();
			var innerDimension = isHorizontal ? me.width : me.height;
			var offset = (isHorizontal ? pixel - me.left : me.bottom - pixel) / innerDimension;
			return me.start + ((me.end - me.start) * offset);
		},
		getPixelForTick: function(index) {
			return this.getPixelForValue(this.ticksAsNumbers[index]);
		}
	});
	Chart.scaleService.registerScaleType('kscale', KScale, defaultConfig);

}).call(this)
