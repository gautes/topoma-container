function bandplot(datasets, kpoints, ctx) {
  var xmax = datasets[0].data[datasets[0].data.length-1].x;
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
     datasets:datasets,  
     labels:kpoints,  
    },
    options: {
         elements: {
            line: {
                tension: 0, // disables bezier curves
               'fill':false, borderWidth:1, 'borderColor':'#4FC3F7'         
               //'fill':false,  'pointRadius':1, 'borderColor':'black'         
                  },
            point:{radius:0}
        },

       tooltips: { mode: 'nearest',
                   callbacks: { label: function(tooltipItem, myChart){
                              return  'band '+ (tooltipItem.datasetIndex+1).toString() +' E='+ tooltipItem.yLabel +' eV' + '  k=' + kpoints[tooltipItem.index].k  ; 
                            } } },
       scales: { 
            xAxes:[{
                type:'kscale',
                id: 'x-axis-0',
                    ticks: { min: 0, max: xmax, } }],
            yAxes:[{
                type:'linear',
                id: 'y-axis-0',
                gridLines:{zeroLineColor:"#ff1744" },
                ticks:{ min: -6, max: 2, } }],
               }, 
       legend: { display: false, },
          zoom: { enabled : true, mode: 'y', rangeMin:{y:-20},rangeMax:{y:10}  },
          pan: { enabled : true, mode: 'y', rangeMin:{y:-20},rangeMax:{y:10}  },
       } 
  });
}

