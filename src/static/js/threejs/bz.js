function bzplot(bz, points, kpoints, bzcanvas){
	var width = 450;
	var height = 450;
	var SCALING = 20;
	var POINTSIZE = 3;
	// var bgcolor =  0xffffff;
	var bgcolor =  0x000000;
	var linecolor =  0xf0f0f0;
	// var linecolor =  0xffffff;
	var textcolor =  0xff1744;
	var pointcolor =  0xff1744;
	var pathcolor =  0x4fc3f7;
	var camera, scene, renderer;
	var textlabels = [];
	init();
	animate();
	function init() {
		var container, separation = 100, amountX = 50, amountY = 50,
		    particles, particle;
		container = document.getElementById( bzcanvas );
		width=container.offsetWidth-30;
		height=width;
		camera = new THREE.OrthographicCamera(- width/2 , width / 2, height / 2, -height / 2, 0.1, 10000 );
		camera.position.z = 1000;
		scene = new THREE.Scene();
		renderer = new THREE.WebGLRenderer({antialias: true});
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( width, height );
		renderer.setClearColor( bgcolor );

		controls = new THREE.OrbitControls( camera, renderer.domElement );
		controls.addEventListener( 'change', render ); // remove when using animation loop
		controls.enableZoom = true;
				
		container.appendChild( renderer.domElement );
                // light 
                // renderer.shadowMap.enabled = true;
				// renderer.shadowMap.type = THREE.PCFSoftShadowMap;
				// renderer.gammaInput = true;
				// renderer.gammaOutput = true;
                // var dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
				// dirLight.color.setHSL( 0.1, 1, 0.95 );
				// dirLight.position.set( -1, 1.75, 1 );
				// dirLight.position.multiplyScalar( 30 );
				// scene.add( dirLight );
				// dirLight.castShadow = true;
				// dirLight.shadow.mapSize.width = 2048;
				// dirLight.shadow.mapSize.height = 2048;
				// var d = 50;
				// dirLight.shadow.camera.left = -d;
				// dirLight.shadow.camera.right = d;
				// dirLight.shadow.camera.top = d;
				// dirLight.shadow.camera.bottom = -d;
				// dirLight.shadow.camera.far = 3500;
				// dirLight.shadow.bias = -0.0001;
				// dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 )
				// scene.add( dirLightHeper );
		// floor
		// var matFloor = new THREE.MeshPhongMaterial();
		// 	var matBox = new THREE.MeshPhongMaterial( { color: 0xaaaaaa } );
		// 	var geoFloor = new THREE.BoxGeometry( 2000, 1, 2000 );
		// 	var geoBox = new THREE.BoxGeometry( 3, 1, 2 );
		// 	var mshFloor = new THREE.Mesh( geoFloor, matFloor );
		// 	var mshBox = new THREE.Mesh( geoBox, matBox );
                                // matFloor.color.set( 0x808080 );
		// 		mshFloor.receiveShadow = true;
		// 		mshFloor.position.set( 0, -50, 0 );
		// 		scene.add( mshFloor );
		// 		// scene.add( mshBox );



		// particles
		var PI2 = Math.PI * 2;
		var material = new THREE.SpriteMaterial( {
					color: pointcolor,
					// program: function ( context ) {
					// 	context.beginPath();
					// 	context.arc( 0, 0, 0.5, 0, PI2, true );
					// 	context.fill();
					// }
					} );


		var linematerial = new THREE.LineBasicMaterial( { color: linecolor, opacity: 0.5, linewidth: 2 } );
		var pathmaterial = new THREE.LineBasicMaterial( { color: pathcolor, opacity: 0.5, linewidth: 1 } );

		/* bz */ 
		for ( var i=0; i<bz.length; i++){
			var geometry = new THREE.Geometry();
			for ( var j=0; j<bz[i].length; j++) {
				particle = new THREE.Sprite( material );
				particle.position.x = bz[i][j][0];
				particle.position.y = bz[i][j][1];
				particle.position.z = bz[i][j][2];
				particle.position.multiplyScalar(  SCALING );
				geometry.vertices.push( particle.position );

			}
			// add the first one to close the face
			particle = new THREE.Sprite( material );
			particle.position.x = bz[i][0][0];
			particle.position.y = bz[i][0][1];
			particle.position.z = bz[i][0][2];
			particle.position.multiplyScalar(  SCALING );
			geometry.vertices.push( particle.position );
                               
			var line = new THREE.Line( geometry, linematerial );
                        // line.castShadow= true ;
			scene.add( line );
		}

		/* high sym points */
		for ( var i=0; i<points.length; i++){
			particle = new THREE.Sprite( material );
			particle.position.x = points[i].x;
			particle.position.y = points[i].y;
			particle.position.z = points[i].z;
			particle.position.multiplyScalar(  SCALING );
			particle.scale.x = particle.scale.y = POINTSIZE;
			scene.add( particle );
			lab=createTextLabel(points[i].label);
			lab.setParent(particle);
			textlabels.push(lab);
			container.appendChild(lab.element);
		}
 
               /* path */
		var geometry = new THREE.Geometry();
		for ( var i=0; i<kpoints.length; i++){
                       var found = false;
                       k = kpoints[i].k;
                       label = kpoints[i].label;
		       for ( var j=0; j<points.length; j++){
                            lab=points[j].label;
                            if ( lab == label ){
				particle = new THREE.Sprite( material );
				particle.position.x = points[j].x;
				particle.position.y = points[j].y;
				particle.position.z = points[j].z;
				particle.position.multiplyScalar(  SCALING );
                                found = true;
				geometry.vertices.push( particle.position );
                           }
                       }
                      if ( found ){ 
                      if ( i == kpoints.length -1 ) {
			      geometry.vertices.push( particle.position ); 
                      //         console.log( label, particle.position );
                      //         console.log('line');
		              var line = new THREE.Line( geometry, pathmaterial );
                              scene.add( line );
		      }  else if ( label != kpoints[i+1].label && kpoints[i+1].label != "" ){
                      //         console.log( label, kpoints[i+1].label );
			      geometry.vertices.push( particle.position ); 
                      //         console.log( label, particle.position );
		              var line = new THREE.Line( geometry, pathmaterial );
                      //         console.log('line');
                              scene.add( line );
		              var geometry = new THREE.Geometry();
                      } else if (label != kpoints[i+1].label) {
			      geometry.vertices.push( particle.position ); 
                      //         console.log( label, particle.position );
                      }}
               }

		window.addEventListener( 'resize', onWindowResize, false );


		function onWindowResize(){
			width=container.offsetWidth-30;
			height=width;
			renderer.setSize( width, width );
		}			
	}
	function animate() {
		requestAnimationFrame( animate );
		render();
	}
	function render() {
		for(var i=0; i<textlabels.length; i++) {
			textlabels[i].updatePosition();
		}
		renderer.render( scene, camera );
	}



  
	function createTextLabel(text) {
		var div = document.createElement('div');
		div.className = 'text-label';
		div.style.position = 'absolute';
		div.style.width = '100px';
		div.style.height = '100px';
                div.style.color = textcolor;
		div.innerHTML = text;
		if(text == '\\Gamma'){
			div.innerHTML = "&Gamma;";  }
		if(text == '\\Sigma'){
			div.innerHTML = "&Sigma;";  }
		if(text == '\\Sigma_1'){
			div.innerHTML = "&Sigma;<sub>1</sub>";  }
		div.style.top = -1000;
		div.style.left = -1000;
		return {
                 element: div,
                 parent: false,
		 position: new THREE.Vector3(0,0,0),
		 setHTML: function(html) {
			 this.element.innerHTML = html;
	         	 },
                 setParent: function(threejsobj) {
		   this.parent = threejsobj;
		   var coords2d = this.get2DCoords(this.position, camera);
	                },
                 updatePosition: function() {
			if(parent) {
				this.position.copy(this.parent.position);
			}
                        var coords2d = this.get2DCoords(this.position, camera);
			this.element.style.left =  coords2d.x + 'px';
			this.element.style.top =  coords2d.y + 'px';
				 },
                 get2DCoords: function(position, camera) {
		      var vector = position.project(camera);
		      vector.x = (vector.x +0.72 )/2.0 * width;
		      vector.y = -(vector.y -1 )/2.0 * height;
		      return vector;
			      }
		};
	}

}
