from django import template
from django.utils.safestring import mark_safe




register = template.Library()

#   to be  improved .....
def threeint(i):
  return "%03d"%(int(i))

# return the link to the space group on ITT website 
def itturl(spacegroup):
   itturl='http://it.iucr.org/Ab/ch7o1v0001/sgtable7o1o' 
   return itturl+"%03d"%(int(spacegroup))

def triangle( context, name ):
   orderby=context['orderby']
   order=context['order']
   if(orderby!=name) :
       return mark_safe("&rtri;")
   elif(order=='desc'):
       return mark_safe("&dtrif;")
   else:
       return mark_safe("&utrif;")

def opposite( context):
   order=context['order']
   if(order == 'asc'):
      return 'desc'
   else:
      return 'asc'

@register.simple_tag
def query_transform(request, **kwargs):
    updated = request.GET.copy()
    for k, v in kwargs.items():
        updated[k] = v
    return updated.urlencode()

@register.simple_tag
def query_reverse(request,orderby):
    updated = request.GET.copy()
    if('order' in updated):
     if(updated['order']=='asc'):
        updated['order']='desc'
     else:
        updated['order']='asc'
    else:
      updated['order']='desc'
    updated['orderby']=orderby
    return updated.urlencode()


def tick(  var ):
   if(var==True):
       return mark_safe('&#10004;')
   else:
       return mark_safe("&#10007;")

def dim(  var ):
   if(var==1 or var=='1'):
       return mark_safe('1D')
   elif(var == 2 or var == '2') :
       return mark_safe("2D")
   elif(var == 3 or var == '3') :
       return mark_safe("3D")
   else:
       return mark_safe("Unknown")

def formula( input_string ):
  res=''
  for char in input_string:
     if(char.isdigit()):
       res+='<sub>'+char+'</sub>'
     else:
       res+=char
  return res

# just to correct COD / OCD
def source( input_string ):
  res=input_string
  if(input_string=='OCD'): 
      res='COD'

  return res

@register.simple_tag(takes_context=True)
def short_list( context, paginator, adjacent_pages=3):
    startPage = max(context['page'] - adjacent_pages, 1)
    if startPage <= 3: startPage = 1
    endPage = context['page'] + adjacent_pages + 1
    if endPage >= paginator.num_pages - 1: endPage = paginator.num_pages + 1
    page_numbers = [n for n in range(startPage, endPage) \
            if n > 0 and n <= paginator.num_pages]
    if 1 not in page_numbers:
            page_numbers.insert(0,-1) # tag to be replaced by '...'
            page_numbers.insert(0,1)
    if paginator.num_pages !=1 and paginator.num_pages not in page_numbers:
            page_numbers.append(-1)
            page_numbers.append(paginator.num_pages)

    return page_numbers
  
register.filter('threeint', threeint)
register.filter('formula', formula, is_safe=True)
register.filter('source', source, is_safe=True)
register.filter('itturl', itturl)
register.filter('tick', tick,is_safe=True)
register.filter('dim', dim,is_safe=True)
register.simple_tag( triangle , takes_context=True)
register.simple_tag( opposite , takes_context=True)
