from django.conf.urls import url
from . import views
# from wkhtmltopdf.views import PDFTemplateView

app_name = 'materials'


urlpatterns = [
    # url(r'^$', views.HomeView.as_view(), name='home'),
    # url(r'^about/$', views.AboutView.as_view(), name='about'),
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^plot/$', views.plot, name='plot'),
    url(r'^search/$', views.search, name='search'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^logout/$', views.logout_view, name='logout'),
]
