import datetime
from django.utils import timezone
from django.db import models


class Material(models.Model):
    formula    = models.CharField(max_length=200)
    elements   = models.CharField(max_length=200,default='')
    nel        = models.IntegerField(default=0)
    icsd       = models.IntegerField(default=0)
    source     = models.CharField(max_length=6,default='ICSD')
    spacegroup = models.CharField(max_length=20)
    spg        = models.IntegerField(default=0)
    pub_date   = models.DateTimeField('date published')
    nat        = models.IntegerField(default=0)
    ne         = models.IntegerField(default=0)
    zmax         = models.IntegerField(default=0)
    gap        = models.FloatField(default=0.0)
    dirgap     = models.FloatField(default=-1.0)
    metallicity = models.FloatField(default=-1.0)
#    volume = models.FloatField(default=0.0)
    matdir     = models.CharField(max_length=500,default='')
    centro     = models.BooleanField(default=False)
    nu         = models.CharField(max_length=7,default='?')
    dim        = models.IntegerField(default=0)
    bandsimage = models.ImageField(default='noimg.png')
    wccimage = models.ImageField(default='noimg.png')
    ciffile  = models.FileField(default='nocif.cif')
    scfin    = models.FileField(default='noscf.in')
    bandsin  = models.FileField(default='nobands.in')
    scfout   = models.FileField(default='noscf.out')
    bandsout = models.FileField(default='nobands.out')
    bandjson = models.FileField(default='nobands.json')
    isti     = models.BooleanField(default=False)
    isweyl   = models.BooleanField(default=False)
    isdirac  = models.BooleanField(default=False)
    selected = models.BooleanField(default=False)
    a        = models.FloatField(default=0.0)
    b        = models.FloatField(default=0.0)
    c        = models.FloatField(default=0.0)
    alpha    = models.FloatField(default=0.0)
    beta     = models.FloatField(default=0.0)
    gamma    = models.FloatField(default=0.0)
    lattice  = models.CharField(max_length=50,default='unknown')
    comment  = models.TextField(default='')
# for this should create a publication class
    publication = models.TextField(default='')

    def __str__(self):              # __unicode__ on Python 2
        return self.formula
    def has_bands(self):
       return self.bandsimage.name != 'noimg.png'
    def has_wcc(self):
        return self.wccimage.name != 'noimg.png'
    def has_cif(self):
        return self.wccimage.name != 'nocif.cif'
    def space_group_number(self):
        return int(self.spacegroup.split()[1].replace('(','').replace(')',''))

