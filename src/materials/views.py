from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.views import generic
from django.core.paginator import Paginator,  EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required,user_passes_test
from django.utils.decorators import method_decorator
from django.db.models import Q
from django.conf import settings
from .models import Material
from django.db import models

DEFAULT_ORDERBY='formula'
DEFAULT_PAGINATE_BY=50
DEFAULT_PAGE=1
DEFAULT_ORDER='asc'
DEFAULT_MAX_LIST=['10','25','50','100']
FILTERS={'ltnel':'','gtnel':'','ltdgap':'','gtdgap':'',
         'centro':'','sel':'','nto':'','source':'all',
         'ltmet':'','gtmet':'','gtgap':0.0,'ltgap':'',
         'gtnat':'','ltnat':'','elements':'','nu':'',
         'topo':'all','icsd':'','spg':'','compo':'','dim':''}

FAMILIES={'TM3D':['Sc','Ti','V' ,'Cr','Mn','Fe','Co','Ni','Cu','Zn'],
'TM4D':['Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd' ],
'TM5D':['La','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg' ],
'C1': ['Li','Na','K','Rb','Cs','XX','XX','XX','XX','XX' ],
'C2':['Be','Mg','Ca','Sr','Ba','XX','XX','XX','XX','XX'],
'C3':['Sc','Y','La' ,'XX','XX','XX','XX','XX','XX','XX'],
'C4':['Ti','Zr','Hf','XX','XX','XX','XX','XX','XX','XX'],
'C5':['V', 'Nb','Ta','XX','XX','XX','XX','XX','XX','XX'],
'C6':['Cr','Mo','W' ,'XX','XX','XX','XX','XX','XX','XX'],
'C7':['Mn','Tc','Re','XX','XX','XX','XX','XX','XX','XX'],
'C8':['Fe','Ru','Os','XX','XX','XX','XX','XX','XX','XX'],
'C9':['Co','Rh','Ir','XX','XX','XX','XX','XX','XX','XX'],
'C10':['Ni','Pd','Pt','XX','XX','XX','XX','XX','XX','XX'],
'C11':['Cu','Ag','Au','XX','XX','XX','XX','XX','XX','XX'],
'C12':['Zn','Cd','Hg','XX','XX','XX','XX','XX','XX','XX'],
'C13':['B','Al','Ga','In','Tl','XX','XX','XX','XX','XX'],
'C14':['C','Si','Ge','Sn','Pb','XX','XX','XX','XX','XX'],
'C15':['N','P' ,'As','Sb','Bi','XX','XX','XX','XX','XX'],
'C16':['O','S' ,'Se','Te','Po','XX','XX','XX','XX','XX'],
'C17':['F','Cl','Br','I','XX','XX','XX','XX','XX','XX'],
}


class HomeView(generic.TemplateView):
    template_name = 'materials/home_base.html'


class AboutView(generic.TemplateView):
    template_name = 'materials/about_base.html'


class IndexView(generic.ListView):
    model = Material
    # template_name = 'materials/index.html'
    template_name = 'materials/index_base.html'
    context_object_name = 'material_list'

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super(IndexView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        # pagination and ordering
        context['order'] = self.request.GET.get('order',DEFAULT_ORDER)
        context['orderby'] = self.request.GET.get('orderby',DEFAULT_ORDERBY)
        context['max'] = self.request.GET.get('max',DEFAULT_PAGINATE_BY)
        context['username'] = self.request.user.get_username()
        context['maxlist'] = DEFAULT_MAX_LIST
        context['plot'] = True
        page = self.request.GET.get('page',DEFAULT_PAGE)
        try:
           context['page']=int(page)
        except:
           context['page']=DEFAULT_PAGE
        # filters
        for k,v in FILTERS.items():
            context[k]=self.request.GET.get(k,'')
        return context

    def get_queryset(self):
#   get ordering and pagination
        orderby=self.request.GET.get('orderby',DEFAULT_ORDERBY)
#       check order can be used to order
        fields=[f.name for f in Material._meta.get_fields() ]
        if(orderby not in fields):
          orderby=DEFAULT_ORDERBY
        order=self.request.GET.get('order',DEFAULT_ORDER)
        ordering='-'+orderby if( order == 'desc' ) else orderby
#       filters
        filters={}
        for k,v in FILTERS.items():
            filters[k]=self.request.GET.get(k,'')
#       check filter type
        try:
          gtgap=float(filters['gtgap'])
        except:
          gtgap=FILTERS['gtgap']
        try:
          ltgap=float(filters['ltgap'])
        except:
          ltgap=FILTERS['ltgap']
        try:
          ltnat=int(filters['ltnat'])
        except:
          ltnat=FILTERS['ltnat']
        try:
          gtnat=int(filters['gtnat'])
        except:
          gtnat=FILTERS['gtnat']
        try:
          gtdgap=float(filters['gtdgap'])
        except:
          gtdgap=FILTERS['gtdgap']
        try:
          ltdgap=float(filters['ltdgap'])
        except:
          ltdgap=FILTERS['ltdgap']
        try:
          ltnel=int(filters['ltnel'])
        except:
          ltnel=FILTERS['ltnel']
        try:
          gtnel=int(filters['gtnel'])
        except:
          gtnel=FILTERS['gtnel']
        try:
          ltmet=float(filters['ltmet'])
        except:
          ltmet=FILTERS['ltmet']
        try:
          gtmet=float(filters['gtmet'])
        except:
          gtmet=FILTERS['gtmet']
        try:
          icsd=int(filters['icsd'])
        except:
          icsd=FILTERS['icsd']
        try:
          dim=int(filters['dim'])
        except:
          dim=FILTERS['dim']
        try:
          topo=filters['topo']
        except:
          topo=FILTERS['topo']
#       extract spg, formula and compositions list
        try:
          elist=[e.strip() for e in filters['elements'].split(',')]
        except:
          elist=[]
        try:
          clist=[c.strip() for c in filters['compo'].split(',')]
        except:
          clist=[]
        try:
          spg=filters['spg'].split(',')
          spglist=[]
          for x in spg:
             if('-' in x ):
                   xl=x.split('-')
                   for i in range(int(xl[0]),int(xl[1])+1):
                      spglist.append(i)
             else:
                   spglist.append(int(x))
        except:
          spglist=[]

####### Filter  #######################
        q=Material.objects.order_by(ordering)
        if( gtgap != ''): q=q.filter( gap__gte = gtgap)
        if( ltgap != ''): q=q.filter( gap__lte = ltgap)
        if( gtdgap != ''): q=q.filter( dirgap__gte = gtdgap)
        if( ltdgap != ''): q=q.filter( dirgap__lte = ltdgap)
        if( gtmet != ''): q=q.filter( metallicity__gte = gtmet)
        if( ltmet != ''): q=q.filter( metallicity__lte = ltmet)
        if( gtnat != ''): q=q.filter( nat__gte = gtnat)
        if( ltnat != ''): q=q.filter( nat__lte = ltnat)
        if( gtnel != ''): q=q.filter( nel__gte = gtnel)
        if( ltnel != ''): q=q.filter( nel__lte = ltnel)
        if( icsd != ''): q=q.filter( icsd = icsd )
        if( filters['source'] == 'COD'): q=q.filter( Q(source  = 'COD') | Q(source = 'OCD') )  # because of stupid type in the database -- should be corrected
        if( filters['source'] == 'ICSD'): q=q.filter( source = filters['source'] )
        if( spglist != []): q=q.filter( spg__in = spglist )
        if( filters['nu'] != ''): q=q.filter( nu = filters['nu'] )
        if( topo=='ti'): q=q.filter( isti = True )
        if( topo=='dirac'): q=q.filter( isdirac = True )
        if( topo=='weyl'): q=q.filter( isweyl = True )
        if( topo=='undef'):
             q=q.filter( isweyl = False )
             q=q.filter( isdirac = False )
             q=q.filter( isti = False )
        if( filters['nto'] == 't'):
          q=q.exclude( nu = '(0;000)')
          q=q.exclude( nu = '?')
        if( dim == 1 or dim == 2 or dim == 3): q=q.filter( dim = dim )
        if( filters['sel'] == 't'): q=q.filter( selected = True )
        if( filters['centro'] == 'c'): q=q.filter( centro = True )
        if( filters['centro'] == 'n'): q=q.filter( centro = False )
        for e in elist:
         if(e != ''):
          if(e.startswith('-')):
           q=q.exclude( formula__contains = e.replace('-','') )
          else:
           q=q.filter( formula__contains = e )
        for c in clist:
         if(c != ''):
          if(c.startswith('-')):
           x=c.replace('-','')
           if x in FAMILIES.keys():
             q=q.exclude(Q(elements__contains='-'+FAMILIES[x][0]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][1]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][2]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][3]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][4]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][5]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][6]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][7]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][8]+'-') |
                        Q(elements__contains='-'+FAMILIES[x][9]+'-')
                       )
           else:
             q=q.exclude(elements__contains=c+'-')

          else:
           if c in FAMILIES.keys():
             q=q.filter(Q(elements__contains='-'+FAMILIES[c][0]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][1]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][2]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][3]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][4]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][5]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][6]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][7]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][8]+'-') |
                        Q(elements__contains='-'+FAMILIES[c][9]+'-')
                       )
           else:
             q=q.filter(elements__contains='-'+c+'-')
########################################################################
#       pagination
        paginate_by=DEFAULT_PAGINATE_BY
        paginate_by=self.request.GET.get('max',DEFAULT_PAGINATE_BY)
        try:
          paginate_by=int(paginate_by)
        except:
          paginate_by=DEFAULT_PAGINATE_BY
        paginator=Paginator(q,paginate_by)
#       page
        page = self.request.GET.get('page',DEFAULT_PAGE)
################# put the full list in the sessions ... might not be optimal ######
        pk_list = []
        for pki in q.values_list('pk', flat=True):
             pk_list.append(pki)
        self.request.session['pk_list'] = pk_list
        self.request.session['list_url'] = self.request.get_full_path()

#    results
        try:
          return paginator.page(page)
        except PageNotAnInteger:
        # If page is not an integer, deliver first page.
          return paginator.page(1)
        except EmptyPage:
         # If page is out of range (e.g. 9999), deliver last page of results.
          return paginator.page(paginator.num_pages)




class DetailView(generic.DetailView):
  model = Material
  template_name = 'materials/detail_base.html'
#  template_name = 'materials/detailpdf.html'

  def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        source_url =  self.request.META.get("HTTP_REFERER", "")
        if('materials/?' in source_url):
             context['from_list']=True
        else:
             context['from_list']=False
        context['source_url']=source_url
        context['username'] = self.request.user.get_username()
        return context

  # @method_decorator(login_required)
  # def dispatch(self, *args, **kwargs):
  #     return super(DetailView, self).dispatch(*args, **kwargs)


# @login_required
def search(request):
    search = request.GET.get('search','')
    q=Material.objects.order_by()
    for e in search.split(','):
         if(e != ''):
           q=q.filter( formula__contains = e )
    nres = q.count()
    if nres == 1:
        mat = q.all()[0]
        return HttpResponseRedirect(reverse('materials:detail',args=[mat.pk]))
    else:
        return HttpResponseRedirect("%s?elements=%s"%(reverse('materials:index'),search))

def plot(request):
    pk_list = request.session['pk_list']
    template_name = 'materials/plot_base.html'
    list_url =  request.session["list_url"]
    x = request.GET.get('x','nat')
    y = request.GET.get('y','gap')
    data = []
    if x != 'nu':
      for pk in pk_list:
        mat = Material.objects.get(pk=pk)
        if(mat.nu!="(0;000)" and mat.nu !="(0,000)" and mat.nu !="?"):
            color="rgba(255,23,68,0.4)"
        else:
            color="rgba(0,0,0,0.1)"
        data.append( {'backgroundColor':color,'pk':pk,'label':mat.formula+" "+mat.nu, 'data': [{ 'x':getattr(mat, x) , 'y':getattr(mat, y)}] } )
    else:
      for pk in pk_list:
        mat = Material.objects.get(pk=pk)
   # to be done ...
    return render(request, template_name, {'plot':True,'x':x,'y':y,'data':data,'from_list': list_url != "", 'source_url': list_url, 'user': request.user, 'pk_list' : pk_list } )

