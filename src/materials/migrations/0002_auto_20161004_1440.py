# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='gap',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='nat',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='material',
            name='ne',
            field=models.IntegerField(default=0),
        ),
    ]
