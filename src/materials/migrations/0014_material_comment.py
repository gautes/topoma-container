# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0013_auto_20161011_1431'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='comment',
            field=models.TextField(default=''),
        ),
    ]
