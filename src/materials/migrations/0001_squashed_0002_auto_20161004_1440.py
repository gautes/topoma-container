# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    replaces = [('materials', '0001_initial'), ('materials', '0002_auto_20161004_1440')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('formula', models.CharField(max_length=200)),
                ('icsd', models.IntegerField(default=0)),
                ('spacegroup', models.CharField(max_length=10)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('gap', models.FloatField(default=0.0)),
                ('nat', models.IntegerField(default=0)),
                ('ne', models.IntegerField(default=0)),
            ],
        ),
    ]
