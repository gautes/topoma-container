# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0004_remove_material_gap'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='gap',
            field=models.FloatField(default=0.0),
        ),
    ]
