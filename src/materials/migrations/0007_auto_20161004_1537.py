# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0006_material_bandsfile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='material',
            name='bandsfile',
        ),
        migrations.AddField(
            model_name='material',
            name='bandsimage',
            field=models.ImageField(default='media/no-img.jpg', upload_to='media/'),
        ),
    ]
