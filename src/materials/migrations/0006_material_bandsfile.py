# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0005_material_gap'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='bandsfile',
            field=models.FilePathField(default='foo.png', path='/home/autes/Work/espresso/high_throughput', recursive=True, match='*png'),
        ),
    ]
