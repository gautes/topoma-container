# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-08-11 18:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0024_material_nel'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='bandjson',
            field=models.FileField(default='nobands.json', upload_to=''),
        ),
    ]
