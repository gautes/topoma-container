# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0002_auto_20161004_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='gap',
            field=models.FloatField(default=0.0),
        ),
    ]
