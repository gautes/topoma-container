# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0010_auto_20161007_1613'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='ciffile',
            field=models.FileField(default='nocif.cif', upload_to=''),
        ),
    ]
