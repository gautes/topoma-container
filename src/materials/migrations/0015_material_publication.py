# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0014_material_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='publication',
            field=models.TextField(default=''),
        ),
    ]
