# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0003_auto_20161004_1445'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='material',
            name='gap',
        ),
    ]
