# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0012_auto_20161011_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='a',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='alpha',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='b',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='beta',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='c',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='gamma',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='material',
            name='lattice',
            field=models.CharField(default='unknown', max_length=50),
        ),
    ]
