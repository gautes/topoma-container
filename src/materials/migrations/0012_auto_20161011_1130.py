# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0011_material_ciffile'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='isdirac',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='material',
            name='isti',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='material',
            name='isweyl',
            field=models.BooleanField(default=False),
        ),
    ]
