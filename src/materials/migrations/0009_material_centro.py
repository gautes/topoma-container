# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0008_auto_20161004_1712'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='centro',
            field=models.BooleanField(default=False),
        ),
    ]
