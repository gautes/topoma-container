# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0007_auto_20161004_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='wccimage',
            field=models.ImageField(default='noimg.png', max_length=200, upload_to=''),
        ),
        migrations.AlterField(
            model_name='material',
            name='bandsimage',
            field=models.ImageField(default='noimg.png', max_length=200, upload_to=''),
        ),
    ]
