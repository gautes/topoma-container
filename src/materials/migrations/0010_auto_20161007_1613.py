# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('materials', '0009_material_centro'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='nu',
            field=models.CharField(default='?', max_length=7),
        ),
        migrations.AlterField(
            model_name='material',
            name='bandsimage',
            field=models.ImageField(default='noimg.png', upload_to=''),
        ),
        migrations.AlterField(
            model_name='material',
            name='wccimage',
            field=models.ImageField(default='noimg.png', upload_to=''),
        ),
    ]
