import django
from django.conf.urls import include , url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    # url(r'^login/$', auth_views.login, name='login'),
    url(r'^', include('materials.urls')),
    # url(r'^account/', include('accounts.urls')),
]
#urlpatterns += patterns('django.views.static',(r'^media/(?P<path>.*)','serve',{'document_root':settings.MEDIA_ROOT}), )
#urlpatterns += patterns('django.views.static',(r'^static/(?P<path>.*)','serve',{'document_root':settings.STATIC_ROOT}), )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
